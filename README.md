# monorepos-gitlab-pipeline-demo

Allow builds for multiple applications in [motorepos](https://en.wikipedia.org/wiki/Monorepo)

Sample codes for the solution provided in https://gitlab.com/gitlab-org/gitlab/-/issues/346695#note_1038004785
